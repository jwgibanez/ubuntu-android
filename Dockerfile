# FROM ubuntu:14.04
FROM openjdk:8
MAINTAINER Jan Ibañez (jwgibanez@protonmail.ch)

RUN java -version

# apt
RUN apt-get update
RUN apt-get install -y curl

# install git
RUN apt-get install -y git

# install aws cli
RUN apt-get install -y unzip
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y python-pip
RUN pip install awscli
RUN pip install --upgrade awscli
RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
RUN unzip awscli-bundle.zip
RUN ./awscli-bundle/install -b ~/bin/aws

# see if $PATH contains ~/bin (output will be empty if it doesn't)
# RUN echo $PATH | grep ~/bin
RUN export PATH=~/bin:$PATH

# download android sdk
RUN apt-get install -y wget
RUN wget http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
RUN tar -xvf android-sdk_r24.4.1-linux.tgz

# install all sdk packages
RUN cd android-sdk-linux/tools && echo y | ./android update sdk --no-ui --all --filter tool,extra-android-m2repository,extra-android-support,extra-google-google_play_services,extra-google-m2repository,android-24
RUN cd android-sdk-linux/tools && echo y | ./android update sdk --no-ui --all --filter build-tools-24.0.1

# adb & # aapt
RUN apt-get install -y gcc-multilib lib32z1 lib32stdc++6
